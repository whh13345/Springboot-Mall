package com.qiu.dao;

import com.qiu.entity.Purchase;

/**
 * @author Qiu
 * @email qiudb.top@aliyun.com
 * @date 2020/11/25 10:38
 * @description 采购部 dao层接口
 */
public interface PurchaseDao extends BaseDao<Purchase> {
}
